###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

# General configuration

###
# Helpers
###

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

activate :middleman_simple_thumbnailer

data.days.day_list.each do | d |
  proxy "days/#{d.number}.html", "days/day.html", :locals => { :day => d,
                                                               :title => "Inktober 2016 Day #{d.number}: #{d.keyword}"
                                                             }
end

ignore "days/day.html"

#set :relative_links, true
#activate :relative_assets
activate :dotenv

# Build-specific configuration
configure :build do
  if ENV['HTTP_PREFIX']
    set :http_prefix, ENV['HTTP_PREFIX']
    set :relative_links, false
  end
  # Minify CSS on build
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript
end
activate :deploy do |deploy|
  deploy.deploy_method = :rsync
  deploy.host          = ENV['DEPLOY_HOST']
  deploy.path          = ENV['DEPLOY_PATH']
end
