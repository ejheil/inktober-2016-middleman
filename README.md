# Inktober 2016

This is my little art web site for 2016, yo.  Content and code
are both in this repo.  The theme is just the default Middleman
theme.

## Deploy

use "rake deploy" or "middleman deploy (--build-before)" to deploy.

## Dotenv

Dotenv variables = DEPLOY_HOST, DEPLOY_PATH, and HTTP_PREFIX
(e.g. /inktober2016')

