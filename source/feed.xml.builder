
day_list = data.days.day_list
last = data.days.day_list.sort{ |a, b| a.date.to_date <=> b.date.to_date }.last.date 

xml.instruct!
xml.feed "xmlns" => "http://www.w3.org/2005/Atom" do
  xml.title "Inktober 2016"
  xml.link :href => ENV['ATOM_LINK'] || ''
  xml.link :href => ENV['ATOM_LINK']+'/feed.xml' || '', :rel => "self"
  xml.updated last.to_time.iso8601
  xml.author { xml.name "Ed Heil" }
  xml.id ENV['ATOM_LINK'] || ''
  day_list.reverse.each do | d |
    url = ENV['ATOM_LINK'] + "/days/#{d.number}.html"
    xml.entry do
      xml.title "Inktober Day #{d.number}: #{d.keyword}"
      xml.link "rel" => "alternate", "href" => url
      xml.id url
      xml.published d.date.to_time.iso8601
      xml.updated d.date.to_time.iso8601
      entry_content = <<END
      <h1>
      #{ link_to( "Day #{d.number}: #{d.keyword}", "/days/#{d.number}.html") }
      </h1>
      <p>
      #{ image_tag( d.filename, resize_to: '500x', class: 'main') }
      </p>
END
      xml.content entry_content, :type => 'html'
    end
  end
end
